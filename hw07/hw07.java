
/// CSE 02 HW07 
/// Thomas Paglinco
/// 10/30/18
/// Calculate a Word Tool Program

import java.util.Scanner;

public class hw07 { // main method required for every Java program
	public static void main(String args[]) {
	 Scanner myscanner = new Scanner(System.in);
	String input= sampleText(); 
	System.out.println("Your input is: "+ input); //User input
	String choice = printMenu();
   while(true){
    
    
	if (choice.equals("c")){
		System.out.println("Number of Non-White Space Characters: " + getNumOfNonWSCharacters(input));choice = printMenu();
	}else
	if (choice.equals("w")){
		System.out.println("Number of words: " + getNumOfWords(input));choice = printMenu();
	}else	  
	if (choice.equals("f")){
		System.out.println("Please enter the word you want to find");
		String find = myscanner.nextLine();
		findText(input, find);
		choice = printMenu();
	}else	
	if (choice.equals("r")){
	    replaceExclamtion(input);choice = printMenu();
	}else
	if (choice.equals("s")){
		shortenSpace(input);choice = printMenu();
	  }else
	if(choice.equals("q"))
	{
		break;
	}else {
		System.out.println("Your code is wrong");
		choice = printMenu();
	}
	}
  }
	  public static String sampleText()
	  {
	    Scanner input = new Scanner(System.in);
	    System.out.println("Enter sample text");
	    String sample = input.nextLine();
	    System.out.println("You entered: " + sample);
	    return sample;
	    
	  }
	  public static String printMenu()
	  {
	    Scanner input = new Scanner(System.in);
	    System.out.println("MENU\nc - Number of non-whitespace characters\nw - Number of word\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit");
	    System.out.println("Choose an option");
	    String option = input.nextLine();
	    return option;
	  }
	  public static int getNumOfNonWSCharacters(String sampleText)
	  {
	    
	    int count = 0;
	    for( int i = 0; i < sampleText.length(); i++){
	      if(sampleText.charAt(i)!= ' '){
	        count++;
	      }
	    }
	    return count;
	  }
	  public static int getNumOfWords(String sampleText)
	  {
	    int length = sampleText.length();
	    int count = 0; int position = 0;
	    for (int i = 0; i < sampleText.length(); i++){
	      position = 0;
	      if(sampleText.charAt(i) == ' '){
	        for(int j = i; j < sampleText.length(); j++){
	          if( sampleText.charAt(j)!= ' '){
	            break;
	          }
	          position++;
	        }
	        count++;
	      }
	      i = position + i;
	    }
	    return count;
	  }
	  public static void findText(String text, String find)
	  {
	    int length = text.length();
	    int count = 0;
	    for(int i =0; i < length; i++){
	      if(text.charAt(i) == find.charAt(0)){
	        int k = 0;
	        boolean judge = false;
	        for(int j = i; j - i  < find.length(); k++,j++){
	         if(k < find.length()&&text.charAt(j) != find.charAt(k)){
	         judge = false;
	         break;
	         }else {
	         judge = true;
	          }
	        }
	        if(judge == true){
	          count++;
	        }
	      }
	    }
	    System.out.println("occurs: "+ count);
	  }
	  public static void replaceExclamtion(String text){
	    int length = text.length();
	    int count = 1;
	    int location = 0;
	    String change = "";
	    for(int i = 0;i< length; i++){
	      if(text.charAt(i) == '!'){
	        if(count == 1 && i == 0){
	          change = ".";
	          System.out.print(".");
	          location = i+1;
	          count++;
	        }else if(count ==1){
	          change = text.substring(0,i) + ".";
	          count++;
	          location = i+1;
	          System.out.print(".");
	        }else {
	          change += text.substring(location, i) + ".";
	          location = i+1;
	          System.out.print(".");
	        }
	      }else {
	    	  System.out.print(text.charAt(i));
	      }
	    }
	    
	    if( location <= text.length()-1){
	      change += text.substring(location);
	    }
	    
	    System.out.println();
	  }
	   
	   public static void shortenSpace(String text){
	     String change = "";
	     int counts = 0;
	     int times = 1;
	     int location = 0;
	     for(int i = 0; i < text.length(); i++){
	    	 if(text.charAt(i) == ' '&&i!=text.length()-1)
	    	 {
	    		 System.out.print(" ");
	    		 for(int j = i + 1; j < text.length(); j++)
	    		 {
	    			 if(text.charAt(j)== ' ')
	    			 {
	    				 location++;
	    			 }else {
	    				 break;
	    			 }
	    		 }
	    		 
	    	 }else {
	    		 System.out.print(text.charAt(i));
	    	 }
	    	 
	    	 i = i + location;
	    	 location = 0;
	     }
	     System.out.println();
	   }
}
