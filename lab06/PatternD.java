//CSE 02 Lab 06
///Course.java
///Thomas Paglinco
///10/11/18
/// Pyramid Pattern prints out
import java.util.Scanner;

public class PatternD {
  public static void main (String [] args) {
    Scanner input = new Scanner(System.in); //Define scanner
    int numRows = 0;//initalize input


    System.out.println("How many rows would you like? Pick a number between 1 and 10."); //asks user for input
      boolean correct = false;
      
      while (correct == false){//setting parameters
        if (input.hasNextInt()){
        numRows = input.nextInt();
        if (numRows < 1 || numRows > 10) {
          System.out.println("Out of range");
      }
      else {
        correct = true;
      }
    }
    else{
      System.out.println("Wrong input type.");
      input.next();
    }
  }
        
    for(int i = numRows; i > 0; i--) {
      for(int j = i,k = 1; k <= i; j--, k++) {
       System.out.print(j + " ");
      } 
      System.out.println();
    }
    
  }
}
    
    
    