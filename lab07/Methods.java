
//// CSE 02 Lab08 Methods.java
/// Thpomas Paglinco
/// 10/25/18
/// Creates 4 different methods to make a sentence

import java.util.Scanner;

public class Methods { // main method required for every Java program
  public static void main(String args[]) {
  String subject = Methods.subject();
  String sentence = "The " + Methods.adjective() + " " + subject + " " + Methods.verb() + " the " + Methods.adjective() + " " + Methods.object() + ".";
   System.out.println(sentence);
   int n = 0;
   n = (int)(Math.random()*4) + 1;
   int i = 0;
   while (i < n){
   System.out.println(Methods.ActionSentence(subject));
   i++;
   }
   System.out.println(Methods.Conclusion(subject));
   }
  
  public static String ActionSentence(String subject){
    int n = 0;
    n = (int)(Math.random()*2);
    String action = "";
    switch ( n ){
       case 0:
        action = "This " + subject + " was " + Methods.adjective() + " to " + Methods.adjective() + " " + Methods.object() + ".";
        break;
       case 1:
        action = "It " + Methods.verb() + " the " + Methods.object() + " for the " + Methods.adjective() + " " + Methods.object() + ".";
        break;
     }
    return action;
  }
  
  public static String Conclusion(String subject){
    String conclusion = "What a " + Methods.adjective() + " " + subject + "!";
    return conclusion;
  }
  public static String adjective( ) {
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "fluffy";
        break;
      case 1:
        val = "soft";
        break;
      case 2:
         val = "cold";
         break;
      case 3:
         val = "gentle";
         break;
      case 4:
         val = "savory";
         break;
      case 5:
         val =  "delicious";
         break;
      case 6:
         val = "white";
         break;
      case 7:
         val = "golden";
         break;
      case 8:
         val = "light";
         break;
      case 9:
         val = "clean";
         break;
    }
    return val;
  }
     
  public static String subject( ){
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "bed";
        break;
      case 1:
        val = "sheets";
        break;
      case 2:
         val = "pillow";
         break;
      case 3:
         val = "sunrise";
         break;
      case 4:
         val = "lamp";
         break;
      case 5:
         val = "morning";
         break;
      case 6:
         val = "walk";
         break;
      case 7:
         val = "slippers";
         break;
      case 8:
         val = "robe";
         break;
      case 9:
         val = "massage";
         break;
  }
    return val;
  } 
  public static String verb( ){
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "slept";
        break;
      case 1:
        val = "dreamed";
        break;
      case 2:
         val = "cuddled";
         break;
      case 3:
         val = "imagined";
         break;
      case 4:
         val = "cleaned";
         break;
      case 5:
         val = "fluffed";
         break;
      case 6:
         val = "swept";
         break;
      case 7:
         val = "smelled";
         break;
      case 8:
         val = "touched";
         break;
      case 9:
         val = "jumped";
         break;
  }
    return val;
  }
  public static String object( ){
    int n = 0;
    String val = "";
    n = (int)(Math.random()*10);
    switch ( n ){
      case 0:
        val = "floor";
        break;
      case 1:
        val = "carpet";
        break;
      case 2:
         val = "rug";
         break;
      case 3:
         val = "hardwood";
         break;
      case 4:
         val = "man";
         break;
      case 5:
         val = "woman";
         break;
      case 6:
         val = "child";
         break;
      case 7:
         val = "doctor";
         break;
      case 8:
         val = "husband";
         break;
      case 9:
         val = "wife";
         break;
    }
    return val;
      }
        }
          