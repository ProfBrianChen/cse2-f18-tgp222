//// CSE 02 Welcome Class HW 01
/// THOMAS PAGLINCO
public class WelcomeClass{

  public static void main(String[] args){
    ///prints Hello Class, Lehigh Netork ID, and autobiographic statement
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-T--G--P--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println(" My name is Thomas Paglinco and I am from Denville, NJ. I am an Eagle Scout and I love tennis.");

   } 
  
}