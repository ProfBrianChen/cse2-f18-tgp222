///CSE 002
///Thomas Paglinco
///9/11/18
///Purpose of this application is to run simple calculations and printing out correct numerical outputs.Purpose

public class Arithmetic {
  ///main method requried for every java program
  public static void main (String [] args){

    //Defining the input variables
    int numPants =3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts= 2;//Number of sweatshirts
    double shirtPrice = 24.99; //Cost per sweatshirt
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99;//cost per belt
    double paSalesTax = 0.06; //sales tax rate of PA
    
  //total cost of individual items before tax
    double totalPantsCost= (numPants*pantsPrice); //calculation of total cost of pants before tax
    System.out.println("The total cost of pants is $"+ (numPants*pantsPrice)); //total cost of pants output
    double totalShirtsCost= (numShirts*shirtPrice); //calculation of total cost of sweatshirts before tax
    System.out.println("The total cost of sweatshirts is $" + (numShirts*shirtPrice)); //total cost of sweathsirts output
    double totalBeltCost= (numBelts*beltPrice); //calculation of total cost of belt before tax
    System.out.println("The total cost of belt is $"+ (numBelts*beltPrice)); //total cost of belt output
      
  //sales tax charged buying each kind of item
    System.out.println("The total sales tax charged on pants is $"+ (((int)(totalPantsCost*paSalesTax*100))/100.0)); //sales tax for pants
    System.out.println("The total sales tax charged on shirts is $"+ (((int)(totalShirtsCost*paSalesTax*100))/100.0)); //sales tax for sweatshirts
    System.out.println("The total sales tax charged on belts is $"+ (((int)(totalBeltCost*paSalesTax*100))/100.0)); //sales tax for belts
    
  //Total cost of purchases before tax
    double totalCostBeforeTax=totalPantsCost+totalShirtsCost+totalBeltCost; //total cost of purchases before tax
    System.out.println("The total cost of the items before tax is $"+(((int)(totalCostBeforeTax*100))/100.0)); //total cost of purchases before tax
                       
  //Total sales tax charged on the items
    double totalSalesTax=(totalCostBeforeTax*paSalesTax); //total sales tax of entire total
    System.out.println("The total sales tax is $"+(((int)(totalSalesTax *100))/ 100.0)); //total sales tax of entire total
                       
  //Total paid for this transaction with sales tax
    double totalWithSalesTax=(totalCostBeforeTax+totalSalesTax); //Total cost with sales tax
    System.out.println("The total with tax comes out total $"+ (((int)(totalWithSalesTax*100))/100.0)); //Total with sales tax output
  }
}