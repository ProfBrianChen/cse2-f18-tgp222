///Thomas Paglinco
///CSE 002 hw03 part 2
///09/18/18
///Pyramid.java

import java.util.Scanner;
public class Pyramid {
  //main method required for every java program
  public static void main (String [] args) {
  //
  Scanner myScanner = new Scanner( System.in ); //declare scanner object
  System.out.print("Enter the base length "); //prompt user for base length
  double baseLength = myScanner.nextDouble() ; //accepts the user input
   
  System.out.print("Enter the width length "); //prompt user for width length
  double widthLength = myScanner.nextDouble() ; //accepts the user input
    
  System.out.print("Enter the height length "); //prompt user for height length
  double heightLength = myScanner.nextDouble() ; //accepts the user input
    

  double volume; //total volume
  volume = (int)((baseLength * widthLength * heightLength)/3) ; //formula to calculate the volume of the pyramid
    //formula to calculate volume
  
    
  System.out.println("The total volume of the pyramid is " + volume ); //outputs the total volume
    
    }
  
  }
    