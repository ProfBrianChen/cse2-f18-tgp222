///Thomas Paglinco
///CSE 002 hw03 part 1
///09/18/18
///Convert.java 
///Purpose of application is to calculate how much rain was dropped on average

import java.util.Scanner;
public class Convert {
  //main method required for every java program
  public static void main (String [] args) {
  //
  Scanner myScanner = new Scanner( System.in ); //declare scanner object
  System.out.print("Enter the affected areas in acres: "); //prompt the user for the affected area in acres
  double numAcres = myScanner.nextDouble() ; //accepts the user input
  
  System.out.print("Enter the inches of rainfall in the affected area by average: "); //prompt the user for the rainfall in the affected areas
  double numRainfall = myScanner.nextDouble() ;//accept user input
   
  double squareMile = (numAcres/640); //acres convert to miles
  double inchPerSquareMile = Math.pow((5280*12),2); //convert to inch per square mile
  double cubicInch = 231; //conversion cubic inches to gallons
  double gallonToCubicMileConversion =9.0817 * Math.pow(10,-13); //converison to cubic mile
  double CubicMileRain =(((inchPerSquareMile *numRainfall) /cubicInch) *squareMile * gallonToCubicMileConversion); 
 
  System.out.println("The total amount of rain per cubic mile is " + CubicMileRain + " cubic miles.");
    }
}