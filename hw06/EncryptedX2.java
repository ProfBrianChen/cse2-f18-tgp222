//CSE 02 HW06
///EncryptedX.java
///Thomas Paglinco
///10/23/18
/// Secret Code
import java.util.Scanner;

public class EncryptedX2 {
  public static void main (String [] args) {
    Scanner input = new Scanner(System.in); //Define scanner
    int numRows = 0;
    int numCols = 0;
    
    System.out.println("How many numRowss would you like? Pick a number between 10 and 100."); //asks user for input
      boolean correct = false;
      
      while (correct == false)//setting parameters
      { 
        if (input.hasNextInt())
        {
        numRows = input.nextInt();
        if (numRows < 10 || numRows > 100) 
        {
          System.out.println("Out of range");
      }
      else {
        correct = true;
      }
    }
    else{
      System.out.println("Wrong input type.");
      input.next();
    }
  }
 
    for (int i = 0; i < numRows; i++) {
      for ( int k= 0; k < numRows; k++) {
//         if (k == i || k == (numRows-1-i)){
//           System.out.print(" ");
//         }
//         else {
          System.out.print("*");
        }
        }
      System.out.println();
      }
    }
}