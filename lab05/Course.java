//CSE 02 Lab 05
///Course.java
///Thomas Paglinco
///10/04/18
///Purpose of this application is to ask use for input for what type of classes they are taking
import java.util.Scanner;

public class Course{
  public static void main (String [] args) {
    Scanner input = new Scanner(System.in); //Define scanner
    
    System.out.println("What is the course number?");
      boolean correct = input.hasNextInt();
      int courseNumber = 0;
      while (correct == false){
          input.next();
          System.out.println("Please enter the correct variable");
          correct = input.hasNextInt();
      }
    courseNumber = input.nextInt();
    

    System.out.print("What is the department name?");
      boolean correctString = input.hasNext();
      String department = "";
        while (correctString == false){
            input.next();
            System.out.println("Please enter the correct variable");
            correctString = input.hasNext();
        }
    department = input.next();
    
    System.out.println("How many times does the class meet?");
      boolean correctInt2 = input.hasNextInt();
      int numberTimesMeet = 0;
      while (correctInt2 == false){
          input.next();
          System.out.println("Please enter the correct variable");
          correctInt2 = input.hasNextInt();
      }
    numberTimesMeet = input.nextInt();
  
    
    System.out.println("What time does the class start? (military time in the form xxxx)");
      boolean correctInt3 = input.hasNextInt();
      int classStart = 0;
      while (correctInt3 == false){
          input.next();
          System.out.println("Please enter the correct variable");
          correctInt3 = input.hasNextInt();
      }
    classStart = input.nextInt();
  
    
    System.out.println("What is the instructor's name?");
      boolean correctString2 = input.hasNext();
      String instructorName = "";
        while (correctString2 ==false){
            input.next();
            System.out.println("Please enter the correct variable");
            correctString2 = input.hasNext();
        }
    instructorName = input.next();
    
    System.out.println("How many students are in the class?");
      boolean correctInt4 = input.hasNext();
      int numberStudents = 0;
      while (correctInt4 == false){
          input.next();
          System.out.println("Please enter the correct variable");
          correctInt4 = input.hasNextInt();
      }
    numberStudents = input.nextInt();
    
  System.out.println("The course number is "+ courseNumber); //Print out Course Number
  System.out.println("The department number is "+ department); //Print out Department Number
  System.out.println("The number of times the class meets is "+ numberTimesMeet); //Print out Number times meet
  System.out.println("The class starts at " + classStart); //Print out what time class starts
  System.out.println("The instructor name is "+ instructorName); //Print out Instructor name
  System.out.println("The number of students in the class is "+ numberStudents); //Print out Number of students
  }
  
}
