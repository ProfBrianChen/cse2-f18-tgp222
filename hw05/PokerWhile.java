///CSE 002 HW 05
///Thomas Paglinco
///10/09/18
///Create a program using while loops to calculate poker probabilities
import java.util.Scanner;

public class PokerWhile {
  ///main method requried for every java program
  public static void main (String [] args){

  Scanner myScanner = new Scanner(System.in); //Scanner 
  System.out.print("How many times would you like to draw a card?"); ///Ask user for how many times want to draw a hand
 
  while(!myScanner.hasNextInt()) {
	  System.out.print("Please enter the correct variable: "); //If user does not input coreect variable, will ask again
	  myScanner.next();
  }
  
  /// Delcaration of variables
  int totalHands = myScanner.nextInt();
  
  int fourOfKind = 0;
  int threeOfKind = 0;
  int twoPair = 0;
  int onePair = 0;
  int temporaryCount = 0;
  int count = 0;
  int testCounter = 0;
  
  /// for while stament to generate cards and to check if they repeat
  for (int i = 0; i < totalHands; i++) {
	  int val1 = (int)(Math.random() * 52) + 1;
	  int val2 = (int)(Math.random() * 52) + 1;
	  int val3 = (int)(Math.random() * 52) + 1;
	  int val4 = (int)(Math.random() * 52) + 1;
	  int val5 = (int)(Math.random() * 52) + 1;
	  while (val1 == val2 || val2 == val3 || val3 == val4|| val4 == val5 ||
         val1 == val3 || val1 == val4 || val1 == val5 || val2 == val4 ||
	       val2 == val5 || val3 == val5) {
	      if (val1 == val2 ) {
	        val2 = (int)(Math.random() * 52) + 1;
   	  } else if (val1 == val3) {
       	  val3 =(int)(Math.random() * 52) + 1;
      } else if (val1 == val4) {
          val4 = (int)(Math.random() * 52) + 1;
      } else if (val1 == val5) {
	        val5 = (int)(Math.random() * 52) + 1;
	    } else if (val2 == val3) {
	        val3 = (int)(Math.random() * 52) + 1;		  
	    } else if (val2 == val4) {
          val4 = (int)(Math.random() * 52) + 1;		  
      } else if (val2 == val5) {
	        val5 = (int)(Math.random() * 52) + 1;		  
      } else if (val3 == val4) {
   	      val4 = (int)(Math.random() * 52) + 1;		  
  	  }else if (val3 == val5) {
          val5 = (int)(Math.random() * 52) + 1;		  
      }else if (val4 == val5) {
	        val5 = (int)(Math.random() * 52) + 1;	  
	   }
    }
  }
  
  System.out.println(val1 % 13);
  System.out.println(val2 % 13);
  System.out.println(val3 % 13);
  System.out.println(val4 % 13);
  System.out.println(val5 % 13);
  System.out.println("------");
  
  temporaryCount = 0;
  count = 0;
  testCounter = 0;
  
  if ((val1 % 13) == (val2 % 13)) || ((val1 % 13) == (val3 % 13)) ||
	   ((val1 % 13) == (val4 % 13)) || ((val1 % 13) == (val5 % 13)){}
	  if ((val1 % 13) == (val2 % 13)) {
		  temporaryCount++
	  }
	  if ((val1 % 13) == (val3 % 13)) {
		  temporaryCount++
	  }
	  if ((val1 % 13) == (val4 % 13)) {
		  temporaryCount++  
	  } 
	  if ((val2 % 13) == val3)) {
		  temporaryCount++  
	  }
   }
	if (temporaryCount > count) {
		count = temporaryCount;
		// temporaryCount = 0;
	}
	temporaryCount = 0;
	  if ((val2 % 13) == (val3 % 13) || (val2 % 13) == (val4 % 13) ||
	       (val2 % 13) == (val5 % 13)) {
			  if (val2 % 13 == val3 % 13) {
				  temporaryCount++;
			  }
			  if (val2 % 13 == val4 % 13) {
				  temporaryCount++;
			  } 
			  if (val2 % 13 == val5 % 13) {
				  temporaryCount++;
			  }
		  	}
		  	if (temporaryCount > count) {
		  	  count = temporaryCount;
		  	  //temporaryCount = 0;
		  	} else if (temporaryCount == 1 && count == 1) {
		  		twoPair++;
		  		testCounter++;
		  	}
		  	temporaryCount = 0;
		  	if (val3 % 13 == val4 % 13 || val3 % 13 == val5 % 13) { 
		  		if (val3 % 13 == val4 % 13) {
		  			temporaryCount++;
		  		}
		  		if (val3 % 13 == val5 % 13) {
		  			temporaryCount++;
		  		}
		  	}
		  	if (temporaryCount > count) {
		  		temporaryCount++; //temporaryCount = 0;
		  	} else if (temporaryCount == 1 && count == 1) {
		  		twoPair++
		  		testCounter++;
		  	}
		  	temporaryCount = 0;
		  	if (val4 % 13 == val5 % 13) {
		  		temporaryCount++; //temporaryCount = 0;
		  	}
		  	if (temporaryCount > count) {
		  		count = temporaryCount;//temporaryCount = 0
		  	} 
		    temporaryCount = 0;
		  		
		    if (count == 4) {
		    	fourOfKind++;
		    } else if (count == 3) {
		    	threeOfKind++;
		    } else if (count == 1 && testCounter < 1) {
		    	onePair++;
		    }
	 
	 System.out.println("4 of a Kind: " + fourOfKind);
	 System.out.println("3 of a Kind: " + threeOfKind);
	 System.out.println("2 Pair: " + twoPair);
	 System.out.println("1 Pair: " + onePair);

	 double numOfTimes = totalHands;
	 double probOfFourOfKind = fourOfKind/ numOfTimes;
	 double probOfThreeOfKind = threeOfKind/ numOfTimes;
	 double probOfTwoPair = twoPair/ numOfTimes;
	 double probOfOnePair = onePair/ numOfTimes;
	 
	 System.out.println("The number of loops " + numOfHands);
	 System.out.println("The probability of a 4 of Kind: %5.3f%n", probOfFourKind);
	 System.out.println("The probability of a 3 of Kind: %5.3f%n", probOfThreeKind);
	 System.out.println("The probability of a 2 Pair: %5.3f%n", probOfTwoPair);
	 System.out.println("The probability of a 1 Pair: %5.3f%n", probOfOnerPair);

  }
  }
  