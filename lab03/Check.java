//CSE 02 Lab 03
///Check.java
///Thomas Paglinco
///9/13/18
///Purpose of this application is calculate the original cost of the check, the percentage tip, and the number of ways the check will be split
import java.util.Scanner;

public class Check {
  //main method required for every java program
  public static void main (String [] args) {
  //
  Scanner myScanner = new Scanner( System.in ); //declare scanner object
  System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt the user for the original cost of the check
  double checkCost = myScanner.nextDouble() ; //accepts the user input
  
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ") ; //prompt the user for the tip percentage they wish to pay
  double tipPercent = myScanner.nextDouble() ;//accept user input
  tipPercent /=100; //Convert the percentage into a decimal value
  
  System.out.print("Enter the number of people who went to dinner: "); //promt the user for how many people went to dinner
  int numPeople = myScanner.nextInt() ;// accept user input
    
  double totalCost; //total cost of dinner
  double costPerPerson; //cost per person
  int dollars,  //whole dollar amount of cost
      dimes, pennies; //for storing digits
        //to the right of the decimal point
        //for the cost$
  totalCost = checkCost * (1+ tipPercent) ;
  costPerPerson = totalCost/ numPeople;
  //get the whole amount, dropping decimal
  dollars=(int) costPerPerson;
  //get dimes amount, e.g.,
  // (int) (6.73 * 10) % 10 -> 67 % 10 -> 7
  //  where the % (mod) operator returns the remainder
  // after the division:  583%100 -> 83, 27%5 ->
  dimes= (int) (costPerPerson * 10) % 10;
  pennies= (int) (costPerPerson * 100) % 10;
    
  System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //says how much each person owes

    
  }//end of matin method
}// end of class