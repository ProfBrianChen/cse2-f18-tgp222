///Thomas Paglinco
///CSE 02 ---- HW 08
///November 13, 2018

import java.util.Scanner;

public class hw08{

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in); 
		 //suits club, heart, spade or diamond 
		 String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
		  cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		System.out.println();
		shuffle(cards); 
		printArray(cards); 
		System.out.println();
		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand);
		   index = index - numCards;
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scan.nextInt();
		   }

	}
	
	public static void printArray(String [ ]array) {
		int i = 0;
		while( i < array.length){
			System.out.print(array[i] + " ");
			i++;
		}
	}
	
	public static void shuffle(String []array) {		
		for(int i = 0; i < 60 ; i++){
			int index = (int)(Math.random()*51) + 1;
			String temp = array[0];
			array[0] = array[index];
			array[index] = temp;
		}
	}
	
	public static String[] getHand(String []array, int index, int numCards) {
		int length = 52;
		int temp = numCards;
		while(temp > 52) {
			length += 52;
			temp -= 52;
		}
    
		String []deck = new String[length];
		int j = 0;
		while(j < deck.length){
			deck[j] = array[j % 52];
			j++;
		}
    
		String []cards = new String[numCards];
		int i=0;
		while(i < cards.length){
			cards[i] = deck[index-1-i];
			i++;
		}
		return cards;
	}

}

