///CSE 02 LAB 02
///Cyclometer.java
///Thomas Paglinco
///9/6/18
///Purpose of application is to meaure speed and distance

public class Cyclometer {
 // main method required for every Java program 
 public static void main (String [] args) {
 // our input data
   int secsTrip1=480; // records seconds of trip 1
   int secsTrip2= 3220; // records seconds of trip 2
   int countsTrip1= 1561; // records counts of trip 1
   int countsTrip2= 9037; // records counts of trip 2
   
 // variable constants defined
   double wheelDiameter= 27.0, // wheel diameter in inches
   PI= 3.14159, // value of pi.
   feetPerMile=5280, // conversion of feet to miles
   inchesPerFoot=12, // conversion of inches to feet
   secondsPerMinute=60; // conversion of seconds to minutes
   double distanceTrip1, distanceTrip2, totalDistance; //declares distance variables
   
  // output of stored variables
    System.out.println("Trip 1 took "+
        (secsTrip1/secondsPerMinute)+" minutes and had "+
         countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
         (secsTrip2/secondsPerMinute)+" minutes and had "+
          countsTrip2+" counts.");
 
   //Calculating distances
   //
   //
  distanceTrip1=countsTrip1*wheelDiameter*PI;
  // Above gives distance in inches
  // (for each count, a rotation of wheel travels
  //the diameter in inches times PI)
   distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
   distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
   totalDistance=distanceTrip1+distanceTrip2;
     
   //Printing Distances
   //Print out the output data
   System.out.println("Trip 1 was "+distanceTrip1+" miles");
   System.out.println("Trip 2 was "+distanceTrip2+" miles");
   System.out.println("the total distance was "+totalDistance+" miles");
   
 } //end of main method
} // end of class
