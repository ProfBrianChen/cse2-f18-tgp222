///Thomas Paglinco
///CSE 02 HW09
///November 27, 2018 
///CSE2Linear.java

import java.util.Scanner;

public class CSE2Linear {
public static void main (String[]args) { ///main method
	
  Scanner scan = new Scanner(System.in);
	int arr[] = new int[15];
  
	for(int i = 0; i < 15; i++) { ///for loop that makesure it is the right type of variable
		System.out.println("Input students"+(i+1)+" grades as integers.");
		while(!scan.hasNextInt()) {
			System.out.println("Wrong variable");
			scan.next();
		}
		
		arr[i] = scan.nextInt();
		while(!(arr[i]>= 0) || !(arr[i]<=100) || (i != 0 && arr[i] < arr[i - 1])) { ///for loop that determines if it is in bounds
			System.out.println("Out of bounds");
			while(!scan.hasNextInt()) {
				System.out.println("Wrong variable");
				scan.next();
			}
			
			arr[i] = scan.nextInt();
			
		}
	}
	System.out.println("Please enter a search key");
	int search = scan.nextInt();
	binarysearch(arr,search); ///Goes to binary search method
	System.out.println("Scrambled");
	scramble(arr);
  
	for(int i = 0; i < arr.length; i++){
		System.out.print(arr[i]+" ");
	}
	
	System.out.println("\nPlease enter a search key");
	search = scan.nextInt();
	linearsearch(arr,search); ///Goes to linear search method 
}

public static void binarysearch(int arr[],int search) { ///binary search method
	int low = 0;
	int high = arr.length-1;
	int index = -1;
	int times = 0;
	while(high >= low) {
		times++;
		int mid = (low + high) / 2;
		if (search < arr[mid]) {
			high = mid - 1;
		}else if (search == arr[mid]) {
			index = mid;
			break;
		}else {
			low = mid + 1;
		}
		
	}
	
	if(index == -1){
		System.out.println(search + " was not found in the list with " + times + " iterations");
	}else {
		System.out.println(search + " was found in the list with " + times + " iterations");
	}
}

public static void linearsearch(int arr[],int search) { ///linear search method

	int index = -1;
	int times = 0;
	
	for(int i = 0; i < arr.length; i++){
		times++;
		if(arr[i] == search)
		{
			index = i;
			break;
		}	
	}
	
	if(index == -1){
		System.out.println(search + " was not found in the list with " + times + " iterations");
	}else {
		System.out.println(search + " was found in the list with " + times + " iterations");
	}
}

public static void scramble(int []arr) { ///scramble method
	
	for(int i = 0; i < arr.length; i++){
		int index = (int)(Math.random() * 15);
		int temp = arr[index];
		arr[index] = arr[0];
		arr[0] = temp;
	}
}
}
