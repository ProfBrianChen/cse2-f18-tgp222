///Thomas Paglinco
///CSE 02 HW09
///November 27, 2018 
///RemoveElements.java

import java.util.Scanner;

public class RemoveElements {
	
	public static void main(String [] arg){ ///main method provided by instructor
		Scanner scan=new Scanner(System.in);
	int num[]=new int[10];
	int newArray1[];
	int newArray2[];
	int index,target;
		String answer="";
		do{
	  	System.out.print("Random input 10 ints [0-9]");
	  	num = RandomInput();
	  	String out = "The original array is:";
	  	out += listArray(num);
	  	System.out.println(out);
	 
	  	System.out.print("Enter the index ");
	  	index = scan.nextInt();
	  	newArray1 = delete(num,index);
	  	String out1="The output array is ";
	  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out1);
	 
	      System.out.print("Enter the target value ");
	  	target = scan.nextInt();
	  	newArray2 = remove(num,target);
	  	String out2="The output array is ";
	  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out2);
	  	 
	  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
	  	answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
	  }
	 
	  public static String listArray(int num[]){ ///method that lists the array
		String out="{";
		for(int j=0;j<num.length;j++){
	  	if(j>0){
	    	out+=", ";
	  	}
	  	out+=num[j];
		}
		out+="} ";
		return out;
	  }
  
	  public static int[] RandomInput() { ///method that generates an array
		  int [] arr = new int[10];
		  
		  for(int i = 0; i < arr.length; i++){
			  arr[i] = (int)(Math.random()*10);
		  }
		  return arr;
	  }
	  
	  public static int[] delete(int []arr, int pos) { ///method that makes a copy of the array with one fewer element
		  if(pos < 0 || pos > arr.length - 1){
			  System.out.println("Index is not valid");
			  return arr;
		  }
		  
		  int arr1[] = new int[arr.length - 1];
		  int times = 0;
		  
		  for(int i = 0; i < arr.length;i++){
			  if(i == pos){
				  continue;
			  }
			  arr1[times] = arr[i];
			  times++;
		  }
		  return arr1;
	  }
  
	  public static int[] remove(int list[] , int target) { ///method that deletes all of the elements equal to the targe
		  int length = 0;
		  int index = -1;
		  
		  for(int i = 0; i < list.length; i++){
			  if(list[i] == target){
				  index = i;
				  continue;
			  }
			  length++;
		  }
		  
		  int newarr[] = new int[length];
		  length = 0;
		  for(int i = 0; i < list.length;i++){
			  if(list[i]==target){
				  continue;
			  }
			  newarr[length] = list[i];
			  length++;
		  }
		  if(index == -1){
			  System.out.println("ELement " + target +" is not found");
		  }
		  return newarr;
	  }
	}
